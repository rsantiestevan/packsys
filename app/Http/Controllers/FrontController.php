<?php

namespace Packsys\Http\Controllers;

use Illuminate\Http\Request;

use Packsys\Http\Requests;

class FrontController extends Controller
{
    //
    public function index() 
    {
        return view('index');
    }
    
    //
    public function contact()
    {
        return view('contact');
    }
    
    //
    public function login()
    {
        return view('login');
    }
}
