@extends('layouts.front')

@section('content')

<div class="banner_top">
			<div class="slider">
				<div class="wrapper">
					<div class="agile_banner_text_info">
						<h3 class="editContent">
							<span class="editContent">Taylor </span> Services <label class="editContent">Express</label> </h3>
					</div>
					<!-- Slideshow 3 -->
					<ul class="rslides" id="slider">
						<li><img src="images/banner1.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner2.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner3.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner4.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner5.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner6.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner1.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner2.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner3.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner4.jpg" data-selector="img" alt=""></li>
						<li><img src="images/banner5.jpg" data-selector="img" alt=""></li>
					</ul>
					<!-- Slideshow 3 Pager -->
					<ul id="slider3-pager">
						<li><a href="#"><img src="images/banner11.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner22.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner33.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner44.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner55.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner66.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner11.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner22.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner33.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner44.jpg" data-selector="img" alt=""></a></li>
						<li><a href="#"><img src="images/banner55.jpg" data-selector="img" alt=""></a></li>
					</ul>
				</div>
			</div>
	</div>
	<!-- //banner -->
	<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="container">
			<div class="col-md-4 agileits_banner_bottom_left">
				<div class="agileinfo_banner_bottom_pos">
					<div class="w3_agileits_banner_bottom_pos_grid">
						<div class="col-xs-3 wthree_banner_bottom_grid_left">
							<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
								<i class="fa fa-laptop" aria-hidden="true"></i>
							</div>
						</div>
						<div class="col-xs-9 wthree_banner_bottom_grid_right">
							<h4>Clean and Modern</h4>
							<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
							<div class="agileits-button two">
								<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div class="col-md-4 agileits_banner_bottom_left">
				<div class="agileinfo_banner_bottom_pos">
					<div class="w3_agileits_banner_bottom_pos_grid">
						<div class="col-xs-3 wthree_banner_bottom_grid_left">
							<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</div>
						</div>
						<div class="col-xs-9 wthree_banner_bottom_grid_right">
							<h4>Unique Design</h4>
							<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
							<div class="agileits-button two">
								<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div class="col-md-4 agileits_banner_bottom_left">
				<div class="agileinfo_banner_bottom_pos">
					<div class="w3_agileits_banner_bottom_pos_grid">
						<div class="col-xs-3 wthree_banner_bottom_grid_left">
							<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
								<i class="fa fa-mobile" aria-hidden="true"></i>
							</div>
						</div>
						<div class="col-xs-9 wthree_banner_bottom_grid_right">
							<h4>Fully Responsive</h4>
							<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
							<div class="agileits-button two">
								<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //banner-bottom -->
	<!-- Modal4 -->
	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="modal-info">
						<h4>Deft</h4>
						<img src="images/banner2.jpg" alt=" " class="img-responsive" />
						<h5>Sub Heading here</h5>
						<p class="para-agileits-w3layouts">Duis sit amet nisi quis leo fermentum vestibulum vitae eget augue. Nulla quam nunc, vulputate id urna at, tempor tincidunt
							metus. Sed feugiat quam nec mauris mattis malesuada.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Modal4 -->
	<!--//about -->

	<!-- services -->
	<div class="services" id="services">
		<div class="container">
			<div class="wthree_head_section">
				<h3 class="w3l_header w3_agileits_header two">Our <span>Services</span></h3>
			</div>
			<div class="agile_wthree_inner_grids">
				<div class="col-md-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-map-o" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Bootstrap Menu</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-rocket" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Responsive Design</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-paint-brush" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Graphic Design</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_banner_bottom_left two">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Google Web Fonts</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_banner_bottom_left  two">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-fire" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Branding</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 agileits_banner_bottom_left  two">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-xs-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-video-camera" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">Video Design</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="/single" role="button">Read More »</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //services -->
	<!-- stats -->
	<div class="stats" id="stats">
		<div class="container">
			<div class="col-md-3 w3layouts_stats_left w3_counter_grid">
				<i class="fa fa-smile-o" aria-hidden="true"></i>
				<p class="counter">45</p>
				<h3>Happy Clients</h3>
			</div>
			<div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
				<i class="fa fa-instagram" aria-hidden="true"></i>
				<p class="counter">165</p>
				<h3>Instagram</h3>
			</div>
			<div class="col-md-3 w3layouts_stats_left w3_counter_grid2">
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				<p class="counter">563</p>
				<h3>Completed Work </h3>
			</div>
			<div class="col-md-3 w3layouts_stats_left w3_counter_grid3">
				<i class="fa fa-trophy" aria-hidden="true"></i>
				<p class="counter">245</p>
				<h3>Awards Won </h3>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //stats -->
	<!-- agile_testimonials -->
	<div class="agile_testimonials" id="testimonials">
		<div class="container">
			<div class="wthree_head_section">
				<h3 class="w3l_header w3_agileits_header">Our <span>testimonials</span></h3>
			</div>

			<div class="agile_wthree_inner_grids">
				<div class="test-agile_main_section">
					<div id="owl-demo2" class="owl-carousel">
						<div class="agile">
							<div class="test-grid">

								<i class="fa fa-quote-left" aria-hidden="true"></i>
								<p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
									veniam, quis. Lorem ipsum dolor .</p>
								<div class="test-grid1">
									<div class="test-grid2">

										<img src="images/1.png" alt="" class="img-r">
									</div>
									<div class="test-grid2_text">
										<h4>John Warner</h4>
										<span>Client</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="agile">
							<div class="test-grid">

								<i class="fa fa-quote-left" aria-hidden="true"></i>
								<p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
									veniam, quis. Lorem ipsum dolor .</p>
								<div class="test-grid1">
									<div class="test-grid2">

										<img src="images/2.png" alt="" class="img-r">
									</div>
									<div class="test-grid2_text">
										<h4>Steve Warner</h4>
										<span>Client</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="agile">
							<div class="test-grid">

								<i class="fa fa-quote-left" aria-hidden="true"></i>
								<p class="para-w3-agile">Lorem ipsum dolor sit amet, consectetur adipiscing elit,consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
									veniam, quis. Lorem ipsum dolor .</p>
								<div class="test-grid1">
									<div class="test-grid2">

										<img src="images/3.png" alt="" class="img-r">
									</div>
									<div class="test-grid2_text">
										<h4>James Warner</h4>
										<span>Client</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //agile_testimonials -->	
	<div class="agile_w3_video">
		<div class="video-grid-single-page-agileits">
			<div data-video="zCyB2DQFdA0" id="video"> <img src="images/video.jpg" alt="" class="img-responsive" /> </div>
		</div>
	</div>
	<!-- /map -->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172"
		    style="border:0"></iframe>
	</div>
	<!-- //map -->
	
	@stop
	